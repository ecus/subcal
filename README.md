# README #

Calibration code example for subsurface TEX86H calibration as described in:

Ho, Sze Ling, and Thomas Laepple. “Flat Meridional Temperature Gradient in the Early Eocene in the Subsurface rather than Surface Ocean.” Nature Geoscience 9, no. 8 (2016): 606–610.

### Installation ###

If not yet installed, install the devtools package:

install.packages(devtools)

Then install the subcal package:

devtools::install_bitbucket("ecus/subcal")


ALTERNATIVE (e.g. it the above fails)
on the command line (terminal)

**git clone https://bitbucket.org/ecus/subcal.git**

This creates a directory subcal

To install the package, on the R command line call

**install("PATH_TO_THE_SUBCAL_DIRECTORY/subcal")**


### Usage ###
library(subcal)

#Simple calibration of one value, no constraint 
Calibrate(tex86h.mean=-0.1)

For more examples and help see
?Calibrate